
class users {
  include user-abernard
  include user-amartin
  include user-bpeterson
  include user-cbratton
  include user-dphilbin
  include user-dschrute
  include user-jhalpert
  include user-kkapoor
  include user-kmalone
  include user-mpalmer
  include user-mscott
  include user-omartinez
  include user-pbeesly
  include user-plapin
  include user-shudson
  include user-tflenderson
}

class groups {
  include group-accounting
  include group-sales
  include group-managers
  include group-vsftp
  include group-http
}

node default {
    include cron-puppet
    include users
    include groups
    include privileges::pam_access::base
    include privileges::pam_access::allow_everywhere
    include privileges::pam_access::deny_unspecified
    include privileges::sudo::admins
    include privileges::sudo::mscott
}

node 'machinee.dundermifflin.com' {
    include secondary-group-dirs 
}

node 'machined.dundermifflin.com' {
    include privileges::pam_access::deny_unspecified
}

node 'machinec.dundermifflin.com' { 
    include privileges::ftp 
    include privileges::pam_access::deny_unspecified
} 

node 'machineb.dundermifflin.com' {
    include privileges::http
    include privileges::pam_access::deny_unspecified
}

node 'machinea.dundermifflin.com' {
    include privileges::pam_access::deny_unspecified
}

if versioncmp($::puppetversion,'3.6.1') >= 0 {

    $allow_virtual_packages = hiera('allow_virtual_packages',false)

    Package {
    	allow_virtual => $allow_virtual_packages,
    }
}


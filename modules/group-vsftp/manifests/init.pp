
class group-vsftp {

    group { 'vsftp':  
        name   => 'vsftp',
        ensure => present,
        gid    => 5000
    }

}


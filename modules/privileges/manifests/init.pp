
class privileges::ftp {

    file { '/var/ftp' :
        ensure    => directory,
        owner     => 'root',
        group     => 'vsftp',
        mode      => '770',
        recurse   => true, 
    }

    sudo::conf { 'ftp':
        source   => 'puppet:///modules/privileges/etc/sudoers.d/ftp'
    }

    pam_access::entry { 'mpalmer':
        user   => 'mpalmer',
        origin => 'ALL'
    }

}

class privileges::http {

    file { '/var/http' :
        ensure    => directory,
        owner     => 'apache',
        group     => 'http',
        mode      => '770',
        recurse   => true,
    }

    sudo::conf { 'http':
        source   => 'puppet:///modules/privileges/etc/sudoers.d/http'
    }

    pam_access::entry { 'pbeesly':
        user   => 'pbeesly',
        origin => 'ALL'
    }

    pam_access::entry { 'kkapoor':
        user   => 'kkapoor',
        origin => 'ALL'
    }

    pam_access::entry { 'abernard':
        user   => 'abernard',
        origin => 'ALL'
    }
 

}

class privileges::pam_access::base {
    # By default, the below 2 files exist but are symlinks
    # to a file managed by authconfig; we get around
    # authconfig by overwriting the link with out own file
    # We add pam_access.so to restrict users' access
    
    # This file grants access through login
    file { '/etc/pam.d/system-auth':
       ensure => present,
       source => 'puppet:///modules/privileges/etc/pam.d/system-auth'
    }
    # This file grants access through ssh
    file { '/etc/pam.d/password-auth':
       ensure => present,
       source => 'puppet:///modules/privileges/etc/pam.d/password-auth'
    }
    file { '/etc/security/pwquality.conf':
        ensure => present,
	source => 'puppet:///modules/privileges/etc/security/pwquality.conf'
    }

}

class privileges::pam_access::allow_everywhere  {

    pam_access::entry { 'root-all':
        user       => 'root',
        origin     => 'ALL',
    }

    pam_access::entry { 'dschrute':
        user       => 'dschrute',
        origin     => 'ALL'
    }

    pam_access::entry { 'bpeterson':
        user       => 'bpeterson',
        origin     => 'ALL'
    }

    pam_access::entry { 'mscott':
        user       => 'mscott',
        origin     => 'ALL'
    }
}

class privileges::sudo::admins {
    sudo::conf { 'admins':
        source   => 'puppet:///modules/privileges/etc/sudoers.d/admins'
    }
}

class privileges::sudo::mscott {
    sudo::conf { 'mscott':
        source   => 'puppet:///modules/privileges/etc/sudoers.d/mscott'
    }
}


class privileges::pam_access::deny_unspecified {
    pam_access::entry { 'deny-everyone-else':
        permission => '-',
        user       => 'ALL',
        origin     => 'ALL'
    } 

}

